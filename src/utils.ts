import { Product } from "./types";

import { datatype, commerce } from "faker";

export const fetcher = (url: string) => {
  console.log("---fetching");
  return fetch(url).then((r) => r.json());
};

export const addProduct = async () => {
  const newProduct: Product = {
    id: datatype.uuid(),
    title: commerce.productName(),
    category: commerce.department(),
    price: parseInt(commerce.price()),
  };

  await fetch("http://localhost:3000/products", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(newProduct),
  });
};

export const removeProduct = async (id: string) => {
  await fetch(`http://localhost:3000/products/${id}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const updateProduct = async () => {
  await fetch("http://localhost:3000/products/1", {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      price: 9999,
    }),
  });
};
