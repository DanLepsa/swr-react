import React, { useState } from "react";
import useSWR from "swr";
import { Product } from "./types";
import { addProduct, fetcher, removeProduct } from "./utils";

function App() {
  const [purchasedItems, setPurchasedItems] = useState<Product[]>([]);

  const { data, error, mutate } = useSWR("/products", fetcher, {
    refreshInterval: 30000,
  });

  const handleBuyItem = async (item: Product) => {
    setPurchasedItems([...purchasedItems, item]);

    await removeProduct(item.id);

    await mutate();
  };

  const handleAddProduct = async () => {
    await addProduct();
    await mutate();
  };

  const renderData = (data: Product[]) => {
    return data.map((item) => (
      <div key={item.id}>
        {item.title}
        <button onClick={() => handleBuyItem(item)}>Buy item</button>
      </div>
    ));
  };

  if (error) {
    return <div>failed to load {error}</div>;
  }

  if (!data) {
    return <div>loading...</div>;
  }

  return (
    <div className="App">
      {renderData(data)}
      <button onClick={handleAddProduct}>add product</button>

      <div>
        <h3>Items in your cart are</h3>
        {purchasedItems.map((i, index) => (
          <p key={index}>{i.title}</p>
        ))}
      </div>
    </div>
  );
}

export default App;
