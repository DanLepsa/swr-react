export type Product = {
  id: string;
  title: string;
  category: string;
  price: number;
};
